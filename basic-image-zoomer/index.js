const imageInput = document.getElementById("imageInput");
const zoomer = document.getElementById("zoomer");
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
canvas.width = 600;
canvas.height = 500;

imageInput.addEventListener("change", e => {
  const selectedFile = e.target.files[0];
  const image = document.createElement("img");
  const reader = new FileReader();
  let canvasWidth, canvasHeight;
  image.classList.add("img");
  image.file = selectedFile;
  image.addEventListener("load", e => {
    canvasWidth = e.target.naturalWidth;
    canvasHeight = e.target.naturalHeight;
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    ctx.drawImage(e.target, 0, 0, canvasWidth, canvasHeight);
  });
  reader.onload = (img => e => {
    img.src = e.target.result;
  })(image);
  reader.readAsDataURL(selectedFile);
});

const imagePos = {
  x: null,
  y: null,
  scale: 1,
  reset: function() {
    this.x = null;
    this.y = null;
    this.scale = 1;
  }
};

const mousePos = {
  x: null,
  y: null,
  reset: function() {
    this.x = null;
    this.y = null;
  }
};

const mouseDelta = {
  x: 0,
  y: 0,
  reset: function() {
    this.x = 0;
    this.y = 0;
  }
};

const cursorToggle = {
  area: zoomer,
  drag: function() {
    this.area.classList.add("is-dragging");
  },
  reset: function() {
    this.area.classList.remove("is-dragging");
  }
};

zoomer.addEventListener("wheel", e => {
  imagePos.scale += e.deltaY * -0.001;
  imagePos.scale = Math.min(Math.max(0.125, imagePos.scale), 4);

  canvas.style.transform = `translate(${imagePos.x || 0}px, ${imagePos.y ||
    0}px) scale(${imagePos.scale})`;
});

let mouseDown = false;

const mouseUpHandler = () => {
  mouseDown = false;
  cursorToggle.reset();
};

const mouseDownHandler = e => {
  mouseDown = true;
  mousePos.x = e.x - mouseDelta.x;
  mousePos.y = e.y - mouseDelta.y;
};

const mouseDbClickHandler = e => {
  mousePos.reset();
  imagePos.reset();
  mouseDelta.reset();
  canvas.style.transform = `translate(0px, 0px) scale(1)`;
};

const mouseDragHandler = e => {
  if (!mousePos.x) {
    mousePos.x = e.x;
    return;
  } else if (mousePos.x < e.x) {
    mouseDelta.x = Math.abs(mousePos.x - e.x);
  } else {
    mouseDelta.x = -Math.abs(mousePos.x - e.x);
  }

  if (!mousePos.y) {
    mousePos.y = e.y;
    return;
  } else if (mousePos.y < e.y) {
    mouseDelta.y = Math.abs(mousePos.y - e.y);
  } else {
    mouseDelta.y = -Math.abs(mousePos.y - e.y);
  }

  imagePos.x = imagePos.x ? mouseDelta.x : imagePos.x + mouseDelta.x;
  imagePos.y = imagePos.y ? mouseDelta.y : imagePos.y + mouseDelta.y;

  canvas.style.transform = `translate(${imagePos.x}px, ${imagePos.y}px) scale(${
    imagePos.scale
  })`;
};

const mouseMoveHandler = e => {
  if (mouseDown) {
    mouseDragHandler(e);
    cursorToggle.drag();
  }
};

zoomer.addEventListener("mousemove", mouseMoveHandler, false);
zoomer.addEventListener("mouseup", mouseUpHandler, false);
zoomer.addEventListener("mouseout", mouseUpHandler, false);
zoomer.addEventListener("mousedown", mouseDownHandler, false);
zoomer.addEventListener("dblclick", mouseDbClickHandler, false);
