const myApp = angular.module("myApp", []);
myApp.controller("myController", function($scope) {
  const data = ["A", "B", "C", "D", "E", "F"];
  $scope.data = data;
  $scope.activeId = 1;
  const container = document.querySelector(".container");
  const wrapper = container.firstElementChild;
  let verticalOffset = 1;
  container.addEventListener("wheel", ({ deltaY }) => {
    if (deltaY > 0) {
      verticalOffset += 200;
      $scope.activeId -= 1;
    } else {
      verticalOffset -= 200;
      $scope.activeId += 1;
    }
    console.log($scope.activeId);

    if ($scope.activeId >= data.length - 2) {
      $scope.data = $scope.data.concat($scope.data);
    }
    
    wrapper.style.transform = `translateY(${verticalOffset}px)`;
    $scope.$apply();
  });
});
